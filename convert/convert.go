package convert

import (
	"encoding/json"
	"io"
	"strings"

	"bytes"

	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

const (
	pathOutput = "gl-sast-report.json"
)

// Decodes one native report from the tool (kubesec)
// and returns a KubesecResult
func decodeReport(reader io.Reader) (*KubesecResult, error) {
	var fileReport KubesecResult

	dec := json.NewDecoder(reader)
	// read open bracket
	_, err := dec.Token()
	if err != nil {
		return nil, err
	}

	for dec.More() {
		err := dec.Decode(&fileReport)
		if err != nil {
			return nil, err
		}
	}

	return &fileReport, nil
}

// Convert reads multiple native reports from the tool (kubesec) and transforms
// them into issues (vulnerabilities), as defined in the common module
func Convert(reader io.Reader, prependPath string) (*report.Report, error) {
	dec := json.NewDecoder(reader)
	// read open bracket
	_, err := dec.Token()
	if err != nil {
		return nil, err
	}

	var fileReports []KubesecReport
	for dec.More() {
		var kubesecOutput KubesecOutput
		err := dec.Decode(&kubesecOutput)
		if err != nil {
			return nil, err
		}

		findingsReader := bytes.NewReader(kubesecOutput.Findings)

		findings, err := decodeReport(findingsReader)
		if err != nil {
			return nil, err
		}

		kubesecReport := KubesecReport{kubesecOutput.Filepath, *findings}
		fileReports = append(fileReports, kubesecReport)
	}

	// Create issues from kubesec report
	vulns := make([]report.Vulnerability, 0)

	for _, filereport := range fileReports {
		for _, vulnerability := range filereport.vulnerabilities() {
			selectors := []string{vulnerability.Object, vulnerability.Selector}
			objectSelector := strings.Join(selectors, " ")

			newVuln := report.Vulnerability{
				Category:    metadata.Type,
				Scanner:     metadata.IssueScanner,
				Name:        vulnerability.Selector,
				Message:     objectSelector,
				Description: vulnerability.Reason,
				CompareKey:  vulnerability.compareKey(),
				Severity:    vulnerability.Severity,
				Confidence:  vulnerability.Confidence,
				Location: report.Location{
					File:  vulnerability.Filepath,
					Class: vulnerability.Object,
				},
				Identifiers: vulnerability.identifiers(),
			}

			vulns = append(vulns, newVuln)
		}
	}

	newReport := report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	return &newReport, nil
}
