package convert

import (
	"strings"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

// KubesecOutput maps to the report output of the kubesec tool
type KubesecOutput struct {
	Filepath string `json:"filepath"`
	Findings []byte `json:"findings"`
}

// KubesecReport maps to a single analysis of a manifest file
type KubesecReport struct {
	Filepath string        `json:"filepath"`
	Findings KubesecResult `json:"findings"`
}

// KubesecResult maps to a decoded finding from the kubesec report
type KubesecResult struct {
	Object  string         `json:"object"`
	Message string         `json:"message"`
	Scoring KubesecScoring `json:"scoring"`
}

// KubesecScoring maps to the individual findings grouped by severity
type KubesecScoring struct {
	CriticalSeverity []KubesecVulnerability `json:"critical"`
	InfoSeverity     []KubesecVulnerability `json:"advise"`
}

// KubesecVulnerability maps to the findings returned from a KubesecReport
type KubesecVulnerability struct {
	ID         string
	Object     string
	Selector   string
	Reason     string
	Confidence report.ConfidenceLevel
	Severity   report.SeverityLevel
	Filepath   string
}

func (r *KubesecReport) vulnerabilities() []KubesecVulnerability {
	var vulnerabilities []KubesecVulnerability

	for _, vulnerability := range r.Findings.Scoring.CriticalSeverity {
		vulnerability.Object = r.Findings.Object
		vulnerability.Confidence = report.ConfidenceLevelHigh
		vulnerability.Severity = report.SeverityLevelCritical
		vulnerability.Filepath = r.Filepath
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	for _, vulnerability := range r.Findings.Scoring.InfoSeverity {
		vulnerability.Object = r.Findings.Object
		vulnerability.Confidence = report.ConfidenceLevelHigh
		vulnerability.Severity = report.SeverityLevelInfo
		vulnerability.Filepath = r.Filepath
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	return vulnerabilities
}

// compareKey returns a string used to establish whether two issues are the same.
func (v *KubesecVulnerability) compareKey() string {
	attrs := []string{v.Filepath, v.Object, strings.Replace(v.Selector, " ", "", -1)}
	return strings.Join(attrs, ":")
}

func (v *KubesecVulnerability) identifiers() []report.Identifier {
	value := v.ID
	// ID field was added with v2.11. fallback to `Reason` when needed
	if value == "" {
		value = v.Reason
	}

	return []report.Identifier{
		{
			Type:  "kubesec_rule_id",
			Name:  value,
			Value: value,
		},
	}
}
