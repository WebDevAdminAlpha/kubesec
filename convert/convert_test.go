package convert

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestConvert(t *testing.T) {
	filepath1 := "simple_manifest.yml"
	report1 := `[
    {
      "object": "DaemonSet/fluentd.kube-system",
      "valid": true,
      "message": "Failed with a score of -15 points",
      "score": -15,
      "scoring": {
        "critical": [
          {
						"ID": "HostNetwork",
            "selector": ".spec .hostNetwork == true",
            "reason": "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter"
          },
          {
						"ID": "HostPID",
            "selector": ".spec .hostPID == true",
            "reason": "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration"
          }
        ],
        "advise": [
          {
						"ID": "CapDropAny",
            "selector": "containers[] .securityContext .capabilities .drop",
            "reason": "Reducing kernel capabilities available to a container limits its attack surface"
          }
        ]
      }
    }
  ]`

	filepath2 := "yaml.yaml"
	report2 := `[
    {
      "object": "DaemonSet/fluentd.kube-system",
      "valid": true,
      "message": "Failed with a score of -15 points",
      "score": -15,
      "scoring": {
        "critical": [
          {
            "selector": ".spec .hostNetwork == true",
            "reason": "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter"
          },
          {
            "selector": ".spec .hostPID == true",
            "reason": "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration"
          }
        ],
        "advise": [
          {
            "selector": "containers[] .securityContext .capabilities .drop",
            "reason": "Reducing kernel capabilities available to a container limits its attack surface"
          }
        ]
      }
    }
  ]`

	var vulnerability1 = KubesecOutput{
		Filepath: filepath1,
		Findings: []byte(report1),
	}

	var vulnerability2 = KubesecOutput{
		Filepath: filepath2,
		Findings: []byte(report2),
	}

	var vulnerabilities = make([]KubesecOutput, 0)
	vulnerabilities = append(vulnerabilities, vulnerability1)
	vulnerabilities = append(vulnerabilities, vulnerability2)

	file, _ := json.MarshalIndent(vulnerabilities, "", " ")

	r := ioutil.NopCloser(bytes.NewReader(file))

	scanner := metadata.IssueScanner

	want := &report.Report{
		Version: report.CurrentVersion(),
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostNetwork == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostNetwork == true",
				Description: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
				CompareKey:  "simple_manifest.yml:DaemonSet/fluentd.kube-system:.spec.hostNetwork==true",
				Severity:    report.SeverityLevelCritical,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "HostNetwork",
						Value: "HostNetwork",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostPID == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostPID == true",
				Description: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
				CompareKey:  "simple_manifest.yml:DaemonSet/fluentd.kube-system:.spec.hostPID==true",
				Severity:    report.SeverityLevelCritical,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "HostPID",
						Value: "HostPID",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "containers[] .securityContext .capabilities .drop",
				Message:     "DaemonSet/fluentd.kube-system containers[] .securityContext .capabilities .drop",
				Description: "Reducing kernel capabilities available to a container limits its attack surface",
				CompareKey:  "simple_manifest.yml:DaemonSet/fluentd.kube-system:containers[].securityContext.capabilities.drop",
				Severity:    report.SeverityLevelInfo,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "CapDropAny",
						Value: "CapDropAny",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostNetwork == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostNetwork == true",
				Description: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
				CompareKey:  "yaml.yaml:DaemonSet/fluentd.kube-system:.spec.hostNetwork==true",
				Severity:    report.SeverityLevelCritical,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
						Value: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostPID == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostPID == true",
				Description: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
				CompareKey:  "yaml.yaml:DaemonSet/fluentd.kube-system:.spec.hostPID==true",
				Severity:    report.SeverityLevelCritical,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
						Value: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
					},
				},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "containers[] .securityContext .capabilities .drop",
				Message:     "DaemonSet/fluentd.kube-system containers[] .securityContext .capabilities .drop",
				Description: "Reducing kernel capabilities available to a container limits its attack surface",
				CompareKey:  "yaml.yaml:DaemonSet/fluentd.kube-system:containers[].securityContext.capabilities.drop",
				Severity:    report.SeverityLevelInfo,
				Confidence:  report.ConfidenceLevelHigh,
				Location: report.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []report.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Reducing kernel capabilities available to a container limits its attack surface",
						Value: "Reducing kernel capabilities available to a container limits its attack surface",
					},
				},
			},
		},
		Analyzer:        "kubesec",
		Config:          ruleset.Config{Path: ruleset.PathSAST},
		Remediations:    []report.Remediation{},
		DependencyFiles: []report.DependencyFile{},
	}

	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, want, got)
}

func TestKubeSecVulnerabilitiesIdentifiers(t *testing.T) {
	in := []byte(`
[
  {
    "object": "StatefulSet/web.default",
    "valid": true,
    "fileName": "/tmp/app/score-0-statefulset-no-sec.yml",
    "message": "Passed with a score of 0 points",
    "score": 0,
    "scoring": {
      "advise": [
        {
          "id": "VolumeClaimAccessModeReadWriteOnce",
          "selector": ".spec .volumeClaimTemplates[] .spec .accessModes | index(\"ReadWriteOnce\")",
          "reason": "",
          "points": 1
        },
        {
          "id": "VolumeClaimRequestsStorage",
          "selector": ".spec .volumeClaimTemplates[] .spec .resources .requests .storage",
          "reason": "",
          "points": 1
        },
        {
          "id": "LimitsCPU",
          "selector": "containers[] .resources .limits .cpu",
          "reason": "Enforcing CPU limits prevents DOS via resource exhaustion",
          "points": 1
        },
        {
          "selector": "containers[] .securityContext .runAsUser -gt 10000",
          "reason": "Run as a high-UID user to avoid conflicts with the host's user table",
          "points": 1
        }
      ]
    }
  }
]
`)

	want := []struct {
		IdentifierType  string
		IdentifierValue string
	}{
		{
			IdentifierType:  "kubesec_rule_id",
			IdentifierValue: "VolumeClaimAccessModeReadWriteOnce",
		},
		{
			IdentifierType:  "kubesec_rule_id",
			IdentifierValue: "VolumeClaimRequestsStorage",
		},
		{
			IdentifierType:  "kubesec_rule_id",
			IdentifierValue: "LimitsCPU",
		},
		{
			IdentifierType:  "kubesec_rule_id",
			IdentifierValue: "Run as a high-UID user to avoid conflicts with the host's user table",
		},
	}

	filereport := []KubesecResult{}

	err := json.Unmarshal(in, &filereport)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 1, len(filereport))
	assert.Equal(t, len(want), len(filereport[0].Scoring.InfoSeverity))

	for _, finding := range filereport {
		for i, vulnerability := range finding.Scoring.InfoSeverity {
			assert.Equal(t, string(vulnerability.identifiers()[0].Type), want[i].IdentifierType)
			assert.Equal(t, vulnerability.identifiers()[0].Name, want[i].IdentifierValue)
			assert.Equal(t, vulnerability.identifiers()[0].Value, want[i].IdentifierValue)
		}
	}
}
