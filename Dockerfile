ARG SCANNER_VERSION=2.11.0
ARG KUBESEC_IMAGE_SHA=7c80b36

FROM alpine:3.12.0 as version_check

ARG SCANNER_VERSION
ARG KUBESEC_IMAGE_SHA

RUN apk update && apk add git

RUN sha_from_tag=$(git ls-remote -t https://github.com/controlplaneio/kubesec.git | grep $SCANNER_VERSION | cut -b 1-7) && test $sha_from_tag == $KUBESEC_IMAGE_SHA || (echo "The SHA $sha_from_tag value obtained from the repository tag $SCANNER_VERSION does not match the expected image SHA $KUBESEC_IMAGE_SHA. Please update both the SCANNER_VERSION and KUBESEC_IMAGE_SHA so that these values match" && exit 1)

FROM golang:1.15-alpine AS build
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

# The "kubesec" user (from the kubesec/kubesec image) doesn't have permission to create a
# file in /etc/ssl/certs or /etc/gitconfig, this RUN command creates files that the
# analyzer can modify.
RUN touch /ca-cert-additional-gitlab-bundle.pem && touch /gitconfig

FROM alpine/helm:3.4.2 as helm

ARG KUBESEC_IMAGE_SHA
FROM kubesec/kubesec:$SCANNER_VERSION

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

COPY --from=helm /usr/bin/helm /usr/bin/helm
ENV PATH="/home/app:${PATH}"
COPY --from=build /go/src/app/analyzer /
COPY --from=build --chown=kubesec:kubesec /ca-cert-additional-gitlab-bundle.pem /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem
COPY --from=build --chown=kubesec:kubesec /gitconfig /etc/gitconfig

ENTRYPOINT []
CMD ["/analyzer", "run"]
