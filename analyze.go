package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os/exec"
	"syscall"

	"github.com/kballard/go-shellquote"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/plugin"
)

const (
	pathOutput = "kubesec.json"
)

var vulnerabilities = make([]convert.KubesecOutput, 0)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    "helm-charts-path",
			Usage:   "Optional helm charts path where to run helm on before runing kubesec",
			EnvVars: []string{"KUBESEC_HELM_CHARTS_PATH"},
		},
		&cli.StringFlag{
			Name:    "helm-options",
			Usage:   "Specify helm options",
			EnvVars: []string{"KUBESEC_HELM_OPTIONS"},
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var helmChartsPath string = c.String("helm-charts-path")
	if helmChartsPath != "" {
		var cmdOut []byte
		var err error
		var args []string

		args = append(args, "template", helmChartsPath)
		if c.String("helm-options") != "" {
			helmOptions, err := shellquote.Split(c.String("helm-options"))
			if err != nil {
				log.Errorf("Invalid helm options:\n%s", err)
				return nil, err
			}
			args = append(args, helmOptions...)
		}
		cmd := exec.Command("helm", args...)

		cmd.Dir = path
		cmdOut, err = cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), cmdOut)
		if err != nil {
			log.Errorf("An error occurred while running helm template:\n%s", cmdOut)
			return nil, err
		}
		ioutil.WriteFile(path+"/sast_helm_generated_manifest.yaml", cmdOut, 0644)
	}
	log.Infof("Searching %s for Kubernetes manifests.\n", path)
	manifests := plugin.WalkForYaml(path)
	for _, manifestPath := range manifests {
		var cmdOut []byte
		var err error

		cmdName := "kubesec"
		args := []string{
			"scan",
			path + manifestPath,
		}

		cmd := exec.Command(cmdName, args...)
		cmdOut, err = cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), cmdOut)

		// This command results in an an exit status 2,
		// but it does return the json as expected.
		if err != nil {
			if exitError, ok := err.(*exec.ExitError); ok {
				waitStatus := exitError.Sys().(syscall.WaitStatus)
				if waitStatus.ExitStatus() != 2 {
					// An error occurred while running kubesec
					// (see https://github.com/controlplaneio/kubesec/blob/74a92d6ab378f8334ef4140c7bd5caeb4f22a265/cmd/kubesec/main.go#L30 )
					log.Errorf("An error occurred while running kubesec:\n%s", err)
					return nil, err
				}
			}
		}

		var vulnerability = convert.KubesecOutput{
			Filepath: manifestPath,
			Findings: cmdOut,
		}

		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	file, err := json.MarshalIndent(vulnerabilities, "", " ")

	return ioutil.NopCloser(bytes.NewReader(file)), err
}
