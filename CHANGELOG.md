# Kubesec analyzer changelog

## v2.14.0
- Update report dependency in order to use the report schema version 14.0.0 (!47)

## v2.13.0
- Feat: updates `primary_identifier` value to kubesec rule ID (!46)

## v2.12.0
- Update kubesec to [v2.11.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.11.0) (!45)
    - Move assets in the containers to make them easier to access
    - Fix changelog links
    - Add exit-code override

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!43)

## v2.11.0
- Update helm docker image to v3.4.2 (!42)

## v2.10.0
- Upgrade common to v2.22.0 (!41)
- Update urfave/cli to v2.3.0 (!41)

## v2.9.0
- Update kubesec to v2.7.2 (!40)
- Update logrus, cli golang dependencies to latest versions

## v2.8.0
- Update common and enable disablement of custom rulesets (!39)

## v2.7.2
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!35)

## v2.7.1
- Update helm to [v3.3.1](https://github.com/helm/helm/releases/tag/v3.3.1) (!31)
- Update golang dependencies

## v2.7.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!30)

## v2.6.1
- Update kubesec to [v2.6.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.6.0) (!27)
- Update golang to v1.15
- Update helm docker image

## v2.6.0
- Add scan object to report (!24)

## v2.5.1
- Update kubesec to [v2.5.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.5.0) (!22)

## v2.5.0
- Switch to the MIT Expat license (!21)

## v2.4.0
- Update logging to be standardized across analyzers (!20)

## v2.3.0
- update helm to [3.2.4](https://github.com/helm/helm/releases/tag/v3.2.4) (!18 @phumberdroz)

## v2.2.4
- Update kubesec to [v2.4.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.4.0) (!17)

## v2.2.3
- Add support for helm charts (!8 @agixid)

## v2.2.2
- Remove `location.dependency` from the generated SAST report (!16)

## v2.2.1
- Use Alpine as builder image (!13)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!10)

## v2.1.0
- Add support for custom CA certs (!7)

## v2.0.1
- Move away from use of CI_PROJECT_DIR variable

## v2.0.0
- Initial release
