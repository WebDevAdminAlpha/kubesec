package plugin

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// SourceFiles maps to the list of valid manifests detected
var SourceFiles = []string{}

// WalkForYaml finds valid Kubernetes manifests (only .yml or .yaml)
func WalkForYaml(projectPath string) []string {
	filepath.Walk(projectPath, func(filePath string, info os.FileInfo, err error) error {

		validManifest, err := checkFile(filePath, info)
		if err != nil {
			return nil
		}
		if validManifest {
			projectPath = strings.Replace(projectPath, "./", "", -1)
			filePath = strings.Replace(filePath, projectPath, "", -1)
			SourceFiles = append(SourceFiles, filePath)
		}
		return nil
	})

	return SourceFiles
}

// Match checks if this project can be built by one of our supported builders
func Match(path string, info os.FileInfo) (bool, error) {
	if info.IsDir() {
		return walkDirectory(path)
	}
	return checkFile(path, info)
}

func walkDirectory(projectPath string) (bool, error) {
	foundMatch := false
	filepath.Walk(projectPath, func(filePath string, info os.FileInfo, err error) error {
		validManifest, err := checkFile(filePath, info)
		if err != nil {
			return nil
		}
		if validManifest {
			foundMatch = true
			return io.EOF //Break out as soon as we find a match
		}
		return nil
	})

	return foundMatch, nil
}

// Determine if file is a valid Kubernetes manifest
func checkFile(path string, info os.FileInfo) (bool, error) {
	if filepath.Ext(info.Name()) == ".yml" || filepath.Ext(info.Name()) == ".yaml" {
		content, err := ioutil.ReadFile(path)
		if err != nil {
			return false, nil
		}

		if info.Name() == "Chart.yaml" {
			if strings.Contains(string(content), "apiVersion:") && strings.Contains(string(content), "name:") && strings.Contains(string(content), "version:") && !strings.Contains(string(content), "{{") && !strings.Contains(string(content), "}}") {
				return true, nil
			}
		} else {
			if strings.Contains(string(content), "apiVersion:") && strings.Contains(string(content), "kind:") && !strings.Contains(string(content), "{{") && !strings.Contains(string(content), "}}") {
				return true, nil
			}
		}
	}
	return false, nil
}

func init() {
	plugin.Register("kubesec", Match)
}
